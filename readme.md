# OpenSSL Certificate Verification
Assignment of the 6th laboratory (which is an extension of the [5th](https://gitlab.com/hodulrom/bi/bez/openssl-certificate)). This program posts a secure GET request to [https://fit.cvut.cz/student/odkazy](https://fit.cvut.cz/student/odkazy) and dumps the response into file `odkazy.html`. During the SSL connection, it also downloads the certificate and saves it into a file `fit_cert.pem`.

In addition, it also verifies the certificate by pre-setting a trusted CA certificate locations. After a connection to the server is established, verification is handled by the SSL library and the program just checks the result.

During the SSL connection, the top priority cipher suite is fetched, printed and then forbidden, followed by another SSL connect call. This should force the server not to use it.

After the top priority cipher is forbidden, all cipher suites that the server offers for the connection are fetched and then printed out.

[EDUX](https://edux.fit.cvut.cz/courses/BI-BEZ/labs/06/start)

## Cipher Suite String Explanation
Cipher suite string consists of several algorithm names, separated by dash. The suite should include key exchange algorithm, data encryption algorithm and data integrity check algorithm.

For instance, suppose we get cipher suite like this one
```
ECDHE-RSA-AES256-GCM-SHA384
```
The string consist of five parts:

| Algorithm                                                                      | Meaning                                                                                   |
|--------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| [`ECDHE`](https://en.wikipedia.org/wiki/Elliptic-curve_Diffie%E2%80%93Hellman) | Elliptic-curve Diffie-Hellman algorithm is used for secure key exchange.                  |
| [`RSA`](https://en.wikipedia.org/wiki/RSA_(cryptosystem))                      | RSA cryptosystem of asymmetric keys is used for authorization (public-private key pairs). |
| [`AES256`](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)         | AES-256 block cipher is used to encrypt the transferred data.                             |
| [`GCM`](https://en.wikipedia.org/wiki/Galois/Counter_Mode)                     | Mode of operation of the above block cipher.                                              |
| [`SHA384`](https://en.wikipedia.org/wiki/SHA-2)                                | Hash function to ensure data integrity (i.e. data is not changed during the transfer).    |

## Usage
You can build and run the application via makefile.

### Build
```bash
make
```

### Run
```bash
make run
```
Outputs files:
* `bin/odkazy.html`
* `bin/fit_cert.pem`

### Clean up build files
```
make clean
```

## Return codes
* `0`: Success.
* `1`: Internet address error.
* `2`: Cannot initialize socket.
* `3`: Cannot connect.
* `4`: SSL error.
* `5`: Web page file write error.
* `6`: Certificate file write error.
* `7`: Certificate download error.
* `8`: Certificate has not passed the verification.
