#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <zconf.h>

// Return codes
#define EXIT_SUCCESS 0
#define EXIT_ADDRESS_ERROR 1
#define EXIT_SOCKET_ERROR 2
#define EXIT_CONNECT_ERROR 3
#define EXIT_SSL_ERROR 4
#define EXIT_FPAGE_ERROR 5
#define EXIT_FCERT_ERROR 6
#define EXIT_CERT_ERROR 7
#define EXIT_VERIFY_ERROR 8

// Parameters
#define URL "fit.cvut.cz"
#define PORT "443"
#define REQUEST "GET /student/odkazy HTTP/1.0\r\n\r\n"
#define OUTPUT_FILE "odkazy.html"
#define OUTPUT_CERT "fit_cert.pem"

typedef struct StringList {
	const char * value;
	struct StringList * next;
} StringList;

/**
 * Initializes new list with given string.
 *
 * @param s String to initialize the list with.
 * @return Newly created list.
 */
StringList * StringList_init(const char * s) {
	StringList * list = (StringList *) malloc(sizeof(StringList));
	list->value = s;
	list->next = NULL;

	return list;
}

/**
 * Pushes new string into the list and returns the newly created child.
 *
 * @param list List to push the child into.
 * @param s String to create the new list entry with.
 */
StringList * StringList_push(StringList * list, const char * s) {
	return list->next = StringList_init(s);
}

/**
 * Prints the list from first to last element into given output. Uses formatted output by given parameter.
 *
 * @param list List to print the elements' values of.
 * @param output Output to print to.
 * @param format Format to use. Use %s where the value should be written.
 */
void StringList_printTo(StringList * list, FILE * output, const char * format) {
	for (StringList * current = list; current != NULL; current = current->next) {
		fprintf(output, format, current->value);
	}
}

/**
 * Releases allocated resources by list and its children.
 *
 * @param list List to free.
 */
void StringList_free(StringList * list) {
	if (list->next != NULL) {
		StringList_free(list->next);
	}

	free(list);
}

/**
 * Gets cipher used in the established SSL connection, then reconnects with this cipher forbidden.
 *
 * @param ssl Connected SSL structure.
 */
const char * forceAlternativeCipher(SSL * ssl) {
	const SSL_CIPHER * cipher = SSL_get_current_cipher(ssl);
	const char * cipherName = SSL_CIPHER_get_name(cipher);

	size_t listStrLen = strlen(cipherName) + 6;
	char cipherList[listStrLen];
	strncat(cipherList, "ALL:!", listStrLen);
	strncat(cipherList, cipherName, listStrLen);

	SSL_set_cipher_list(ssl, cipherList);
	SSL_connect(ssl);

	return cipherName;
}

/**
 * Dumps response of the given request, based on where the SSL is connected, to given output.
 *
 * @param ssl Connected SSL structure.
 * @param output Output to dump the response to.
 * @param request Request to send.
 * @return Return code according to the operation result.
 */
int dumpResponse(SSL * ssl, FILE * output, const char * request) {
	if (SSL_write(ssl, request, (int) strlen(request)) <= 0) {
		return EXIT_SSL_ERROR;
	}

	int length = 1;
	unsigned char buffer[1024];

	while (length > 0) {
		length = SSL_read(ssl, buffer, sizeof buffer);

		if (length > 0 && !fwrite(buffer, sizeof(unsigned char), (size_t) length, output)) {
			return EXIT_FPAGE_ERROR;
		}
	}

	fflush(output);

	return EXIT_SUCCESS;
}

/**
 * Dumps SSL certificate, based on where the SSL is connected, to given output.
 *
 * @param ssl Connected SSL structure.
 * @param output Output to dump the certificate to.
 * @return Return code according to the operation result.
 */
int dumpCertificate(SSL * ssl, FILE * output) {
	X509 * certificate = SSL_get_peer_certificate(ssl);

	if (certificate == NULL) return EXIT_CERT_ERROR;

	int status = PEM_write_X509(output, certificate);
	X509_free(certificate);

	if (!status) return EXIT_CERT_ERROR;

	fflush(output);

	return EXIT_SUCCESS;
}

/**
 * Initializes SSL connection to given socket file descriptor.
 *
 * @param sockfd Socket file descriptor to connect to.
 * @return Initialized SSL or NULL on failure.
 */
SSL * initSSL(int sockfd) {
	SSL_library_init();
	SSL_CTX * ctx = SSL_CTX_new(SSLv23_client_method());

	if (ctx == NULL) return NULL;

	// Forbid insecure protocols
	SSL_CTX_set_options(ctx, (unsigned) SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TLSv1);

	// Verification paths to verify certificate
	if (!SSL_CTX_set_default_verify_paths(ctx)) return NULL;

	SSL * ssl = SSL_new(ctx);

	if (!SSL_set_fd(ssl, sockfd)) return NULL;
	if (SSL_connect(ssl) <= 0) return NULL;

	return ssl;
}

/**
 * Prints error to stderr according to the return code. If non-error return code is given, nothing is printed.
 *
 * @param returnCode Return code to print the error of.
 */
void printErrors(int returnCode) {
	if (returnCode == EXIT_SUCCESS) return;

	switch (returnCode) {
		case EXIT_SSL_ERROR:
			fprintf(stderr, "SSL connection to %s:%s failed.\n", URL, PORT);
			ERR_print_errors_fp(stderr);
			break;
		case EXIT_CERT_ERROR:
			fprintf(stderr, "Cannot download certificate from %s:%s\n", URL, PORT);
			ERR_print_errors_fp(stderr);
			break;
		case EXIT_FPAGE_ERROR:
			fprintf(stderr, "Cannot open \"%s\": %s.\n", OUTPUT_FILE, strerror(errno));
			break;
		case EXIT_FCERT_ERROR:
			fprintf(stderr, "Cannot open \"%s\": %s.\n", OUTPUT_CERT, strerror(errno));
			break;
		case EXIT_CONNECT_ERROR:
			fprintf(stderr, "connect error: %s\n", strerror(errno));
			break;
		case EXIT_SOCKET_ERROR:
			fprintf(stderr, "socket error: %s\n", strerror(errno));
			break;
		case EXIT_VERIFY_ERROR:
			fprintf(stderr, "X509 Certificate verification failed.\n");
			break;
	}
}

/**
 * Gets a list of available ciphers' names. First element is the top priority cipher and the rest is sorted by priority
 * in descending order.
 *
 * @param ssl Connected SSL structure.
 * @return List of ciphers.
 */
StringList * getCipherList(SSL * ssl) {
	const char * topCipher = SSL_get_cipher_list(ssl, 0);
	if (topCipher == NULL) return NULL;

	StringList * list = StringList_init(topCipher);
	StringList * child = list;

	for (unsigned i = 1; i < UINT_MAX; ++i) {
		const char * cipherName = SSL_get_cipher_list(ssl, i);
		if(cipherName == NULL) break;

		child = StringList_push(child, cipherName);
	}

	return list;
}

int main(int argc, char * argv[]) {
	int status;
	int sockfd;
	int returnCode;
	struct addrinfo * address;
	struct addrinfo hints;

	// Empty the structure
	memset(& hints, 0, sizeof hints);

	hints.ai_family = AF_UNSPEC; // IP version agnostic
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets

	status = getaddrinfo(URL, PORT, & hints, & address);

	if (status) {
		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
		return EXIT_ADDRESS_ERROR;
	}

	SSL * ssl = NULL;
	FILE * output = NULL;
	FILE * outputCert = NULL;

	do {
		sockfd = socket(address->ai_family, address->ai_socktype, address->ai_protocol);

		if (sockfd == -1) {
			returnCode = EXIT_SOCKET_ERROR;
			break;
		}

		if (connect(sockfd, address->ai_addr, address->ai_addrlen) == -1) {
			returnCode = EXIT_CONNECT_ERROR;
			break;
		}

		ssl = initSSL(sockfd);

		if (ssl == NULL) {
			returnCode = EXIT_SSL_ERROR;
			break;
		}

		if (SSL_get_verify_result(ssl) != X509_V_OK) {
			returnCode = EXIT_VERIFY_ERROR;
			break;
		}

		const char * forbiddenCipher = forceAlternativeCipher(ssl);

		if (forbiddenCipher == NULL) {
			returnCode = EXIT_SSL_ERROR;
			break;
		}

		printf("Forbidding top priority cipher\n%s\n", forbiddenCipher);

		StringList * cipherList = getCipherList(ssl);

		if (cipherList == NULL) {
			returnCode = EXIT_SSL_ERROR;
			break;
		}

		printf("Available ciphers by priority (descending)\n");
		StringList_printTo(cipherList, stdout, "%s\n");
		StringList_free(cipherList);

		output = fopen(OUTPUT_FILE, "wb");

		if (output == NULL) {
			returnCode = EXIT_FCERT_ERROR;
			break;
		}

		outputCert = fopen(OUTPUT_CERT, "wb");

		if (outputCert == NULL) {
			returnCode = EXIT_FPAGE_ERROR;
			break;
		}

		returnCode = dumpCertificate(ssl, outputCert);
		if (returnCode != EXIT_SUCCESS) break;
		returnCode = dumpResponse(ssl, output, REQUEST);
	}
	while (false);

	if (ssl != NULL && !SSL_shutdown(ssl)) {
		// Might need a retry if bidirectional shutdown is required
		if (SSL_shutdown(ssl) <= 0 && returnCode == EXIT_SUCCESS) {
			returnCode = EXIT_SSL_ERROR;
		}
	}

	printErrors(returnCode);
	SSL_CTX_free(SSL_get_SSL_CTX(ssl));
	SSL_free(ssl);
	ERR_free_strings();
	fclose(output);
	fclose(outputCert);
	close(sockfd);
	freeaddrinfo(address);

	return returnCode;
}
